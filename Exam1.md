### Create local constants

> Local constants should be initialized before used

###### Good:

```
final int a = 1;

System.out.println(a);
```

###### So so:

```
final int a;

if(condition)
	a = 1;
else
	a = 2;
 
System.out.println(a);
 ```
 
###### Does not compile:

```
final int a;

if(condition)
	a = 4;
   
System.out.println(a);
```
###### Best practice: Use UPPERCASES to mark constants

```

final int A = 5;

System.out.println(A);

```